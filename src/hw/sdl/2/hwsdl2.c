#include "config.h"

#include <stdio.h>
#include <stdlib.h>

#ifdef HAVE_SDLMAIN
#include "SDL_main.h"
#endif

#include "SDL.h"
#include "SDL_keycode.h"

#include "hw.h"
#include "hwsdl_audio.h"
#include "hwsdl_mouse.h"
#include "hwsdl_opt.h"
#include "hwsdl_video.h"
#include "hwsdl2_defs.h"
#include "hwsdl2_video.h"
#include "kbd.h"
#include "log.h"
#include "main.h"
#include "options.h"
#include "types.h"

/* -------------------------------------------------------------------------- */

const char *idstr_hw = "sdl2";

/* -------------------------------------------------------------------------- */

#define SDLK_TBLI_FROM_SCAN(i) ((i) & (~SDLK_SCANCODE_MASK))

static const mookey_t key_xlat_key[0x80] = {
    [SDLK_BACKSPACE] = MOO_KEY_BACKSPACE,
    [SDLK_TAB] = MOO_KEY_TAB,
    [SDLK_RETURN] = MOO_KEY_RETURN,
    [SDLK_ESCAPE] = MOO_KEY_ESCAPE,
    [SDLK_SPACE] = MOO_KEY_SPACE,
    [SDLK_EXCLAIM] = MOO_KEY_EXCLAIM,
    [SDLK_QUOTEDBL] = MOO_KEY_QUOTEDBL,
    [SDLK_HASH] = MOO_KEY_HASH,
    [SDLK_DOLLAR] = MOO_KEY_DOLLAR,
    [SDLK_AMPERSAND] = MOO_KEY_AMPERSAND,
    [SDLK_QUOTE] = MOO_KEY_QUOTE,
    [SDLK_LEFTPAREN] = MOO_KEY_LEFTPAREN,
    [SDLK_RIGHTPAREN] = MOO_KEY_RIGHTPAREN,
    [SDLK_ASTERISK] = MOO_KEY_ASTERISK,
    [SDLK_PLUS] = MOO_KEY_PLUS,
    [SDLK_COMMA] = MOO_KEY_COMMA,
    [SDLK_MINUS] = MOO_KEY_MINUS,
    [SDLK_PERIOD] = MOO_KEY_PERIOD,
    [SDLK_SLASH] = MOO_KEY_SLASH,
    [SDLK_0] = MOO_KEY_0,
    [SDLK_1] = MOO_KEY_1,
    [SDLK_2] = MOO_KEY_2,
    [SDLK_3] = MOO_KEY_3,
    [SDLK_4] = MOO_KEY_4,
    [SDLK_5] = MOO_KEY_5,
    [SDLK_6] = MOO_KEY_6,
    [SDLK_7] = MOO_KEY_7,
    [SDLK_8] = MOO_KEY_8,
    [SDLK_9] = MOO_KEY_9,
    [SDLK_COLON] = MOO_KEY_COLON,
    [SDLK_SEMICOLON] = MOO_KEY_SEMICOLON,
    [SDLK_LESS] = MOO_KEY_LESS,
    [SDLK_EQUALS] = MOO_KEY_EQUALS,
    [SDLK_GREATER] = MOO_KEY_GREATER,
    [SDLK_QUESTION] = MOO_KEY_QUESTION,
    [SDLK_AT] = MOO_KEY_AT,
    [SDLK_LEFTBRACKET] = MOO_KEY_LEFTBRACKET,
    [SDLK_BACKSLASH] = MOO_KEY_BACKSLASH,
    [SDLK_RIGHTBRACKET] = MOO_KEY_RIGHTBRACKET,
    [SDLK_CARET] = MOO_KEY_CARET,
    [SDLK_UNDERSCORE] = MOO_KEY_UNDERSCORE,
    [SDLK_BACKQUOTE] = MOO_KEY_BACKQUOTE,
    [SDLK_a] = MOO_KEY_a,
    [SDLK_b] = MOO_KEY_b,
    [SDLK_c] = MOO_KEY_c,
    [SDLK_d] = MOO_KEY_d,
    [SDLK_e] = MOO_KEY_e,
    [SDLK_f] = MOO_KEY_f,
    [SDLK_g] = MOO_KEY_g,
    [SDLK_h] = MOO_KEY_h,
    [SDLK_i] = MOO_KEY_i,
    [SDLK_j] = MOO_KEY_j,
    [SDLK_k] = MOO_KEY_k,
    [SDLK_l] = MOO_KEY_l,
    [SDLK_m] = MOO_KEY_m,
    [SDLK_n] = MOO_KEY_n,
    [SDLK_o] = MOO_KEY_o,
    [SDLK_p] = MOO_KEY_p,
    [SDLK_q] = MOO_KEY_q,
    [SDLK_r] = MOO_KEY_r,
    [SDLK_s] = MOO_KEY_s,
    [SDLK_t] = MOO_KEY_t,
    [SDLK_u] = MOO_KEY_u,
    [SDLK_v] = MOO_KEY_v,
    [SDLK_w] = MOO_KEY_w,
    [SDLK_x] = MOO_KEY_x,
    [SDLK_y] = MOO_KEY_y,
    [SDLK_z] = MOO_KEY_z,
};

static const mookey_t key_xlat_scan[SDL_NUM_SCANCODES] = {

    [SDLK_TBLI_FROM_SCAN(SDLK_CAPSLOCK)] = MOO_KEY_CAPSLOCK,

    [SDLK_TBLI_FROM_SCAN(SDLK_F1)] = MOO_KEY_F1,
    [SDLK_TBLI_FROM_SCAN(SDLK_F2)] = MOO_KEY_F2,
    [SDLK_TBLI_FROM_SCAN(SDLK_F3)] = MOO_KEY_F3,
    [SDLK_TBLI_FROM_SCAN(SDLK_F4)] = MOO_KEY_F4,
    [SDLK_TBLI_FROM_SCAN(SDLK_F5)] = MOO_KEY_F5,
    [SDLK_TBLI_FROM_SCAN(SDLK_F6)] = MOO_KEY_F6,
    [SDLK_TBLI_FROM_SCAN(SDLK_F7)] = MOO_KEY_F7,
    [SDLK_TBLI_FROM_SCAN(SDLK_F8)] = MOO_KEY_F8,
    [SDLK_TBLI_FROM_SCAN(SDLK_F9)] = MOO_KEY_F9,
    [SDLK_TBLI_FROM_SCAN(SDLK_F10)] = MOO_KEY_F10,
    [SDLK_TBLI_FROM_SCAN(SDLK_F11)] = MOO_KEY_F11,
    [SDLK_TBLI_FROM_SCAN(SDLK_F12)] = MOO_KEY_F12,

    [SDLK_TBLI_FROM_SCAN(SDLK_SCROLLLOCK)] = MOO_KEY_SCROLLOCK,
    [SDLK_TBLI_FROM_SCAN(SDLK_PAUSE)] = MOO_KEY_PAUSE,
    [SDLK_TBLI_FROM_SCAN(SDLK_INSERT)] = MOO_KEY_INSERT,
    [SDLK_TBLI_FROM_SCAN(SDLK_HOME)] = MOO_KEY_HOME,
    [SDLK_TBLI_FROM_SCAN(SDLK_PAGEUP)] = MOO_KEY_PAGEUP,
    [SDLK_TBLI_FROM_SCAN(SDLK_DELETE)] = MOO_KEY_DELETE,
    [SDLK_TBLI_FROM_SCAN(SDLK_END)] = MOO_KEY_END,
    [SDLK_TBLI_FROM_SCAN(SDLK_PAGEDOWN)] = MOO_KEY_PAGEDOWN,
    [SDLK_TBLI_FROM_SCAN(SDLK_RIGHT)] = MOO_KEY_RIGHT,
    [SDLK_TBLI_FROM_SCAN(SDLK_LEFT)] = MOO_KEY_LEFT,
    [SDLK_TBLI_FROM_SCAN(SDLK_DOWN)] = MOO_KEY_DOWN,
    [SDLK_TBLI_FROM_SCAN(SDLK_UP)] = MOO_KEY_UP,

    [SDLK_TBLI_FROM_SCAN(SDLK_CLEAR)] = MOO_KEY_CLEAR,

    [SDLK_TBLI_FROM_SCAN(SDLK_F13)] = MOO_KEY_F13,
    [SDLK_TBLI_FROM_SCAN(SDLK_F14)] = MOO_KEY_F14,
    [SDLK_TBLI_FROM_SCAN(SDLK_F15)] = MOO_KEY_F15,

    [SDLK_TBLI_FROM_SCAN(SDLK_KP_0)] = MOO_KEY_KP0,
    [SDLK_TBLI_FROM_SCAN(SDLK_KP_1)] = MOO_KEY_KP1,
    [SDLK_TBLI_FROM_SCAN(SDLK_KP_2)] = MOO_KEY_KP2,
    [SDLK_TBLI_FROM_SCAN(SDLK_KP_3)] = MOO_KEY_KP3,
    [SDLK_TBLI_FROM_SCAN(SDLK_KP_4)] = MOO_KEY_KP4,
    [SDLK_TBLI_FROM_SCAN(SDLK_KP_5)] = MOO_KEY_KP5,
    [SDLK_TBLI_FROM_SCAN(SDLK_KP_6)] = MOO_KEY_KP6,
    [SDLK_TBLI_FROM_SCAN(SDLK_KP_7)] = MOO_KEY_KP7,
    [SDLK_TBLI_FROM_SCAN(SDLK_KP_8)] = MOO_KEY_KP8,
    [SDLK_TBLI_FROM_SCAN(SDLK_KP_9)] = MOO_KEY_KP9,
    [SDLK_TBLI_FROM_SCAN(SDLK_KP_PERIOD)] = MOO_KEY_KP_PERIOD,
    [SDLK_TBLI_FROM_SCAN(SDLK_KP_DIVIDE)] = MOO_KEY_KP_DIVIDE,
    [SDLK_TBLI_FROM_SCAN(SDLK_KP_MULTIPLY)] = MOO_KEY_KP_MULTIPLY,
    [SDLK_TBLI_FROM_SCAN(SDLK_KP_MINUS)] = MOO_KEY_KP_MINUS,
    [SDLK_TBLI_FROM_SCAN(SDLK_KP_PLUS)] = MOO_KEY_KP_PLUS,
    [SDLK_TBLI_FROM_SCAN(SDLK_KP_ENTER)] = MOO_KEY_KP_ENTER,
    [SDLK_TBLI_FROM_SCAN(SDLK_KP_EQUALS)] = MOO_KEY_KP_EQUALS,
    [SDLK_TBLI_FROM_SCAN(SDLK_NUMLOCKCLEAR)] = MOO_KEY_NUMLOCK,
    [SDLK_TBLI_FROM_SCAN(SDLK_RSHIFT)] = MOO_KEY_RSHIFT,
    [SDLK_TBLI_FROM_SCAN(SDLK_LSHIFT)] = MOO_KEY_LSHIFT,
    [SDLK_TBLI_FROM_SCAN(SDLK_RCTRL)] = MOO_KEY_RCTRL,
    [SDLK_TBLI_FROM_SCAN(SDLK_LCTRL)] = MOO_KEY_LCTRL,
    [SDLK_TBLI_FROM_SCAN(SDLK_RALT)] = MOO_KEY_RALT,
    [SDLK_TBLI_FROM_SCAN(SDLK_LALT)] = MOO_KEY_LALT,
    [SDLK_TBLI_FROM_SCAN(SDLK_LGUI)] = MOO_KEY_LSUPER,
    [SDLK_TBLI_FROM_SCAN(SDLK_RGUI)] = MOO_KEY_RSUPER,
    [SDLK_TBLI_FROM_SCAN(SDLK_MODE)] = MOO_KEY_MODE,
    [SDLK_TBLI_FROM_SCAN(SDLK_HELP)] = MOO_KEY_HELP,
    [SDLK_TBLI_FROM_SCAN(SDLK_PRINTSCREEN)] = MOO_KEY_PRINT,
    [SDLK_TBLI_FROM_SCAN(SDLK_SYSREQ)] = MOO_KEY_SYSREQ,
    [SDLK_TBLI_FROM_SCAN(SDLK_MENU)] = MOO_KEY_MENU,
    [SDLK_TBLI_FROM_SCAN(SDLK_POWER)] = MOO_KEY_POWER,
    [SDLK_TBLI_FROM_SCAN(SDLK_UNDO)] = MOO_KEY_UNDO,
};

static inline uint32_t mod_xlat(SDL_Keymod smod)
{
    uint32_t mod = 0;
    if (smod & KMOD_SHIFT) { mod |= MOO_MOD_SHIFT; }
    if (smod & KMOD_ALT) { mod |= MOO_MOD_ALT; }
    if (smod & KMOD_CTRL) { mod |= MOO_MOD_CTRL; }
    return mod;
}

static bool hw_textinput_active = false;

/* -------------------------------------------------------------------------- */

#define SDL1or2Key  SDL_Keycode
#define SDL1or2Mod  SDL_Keymod

#include "hwsdl.c"

/* -------------------------------------------------------------------------- */

static void hw_event_handle_window(SDL_WindowEvent *e)
{
    switch (e->event) {
        case SDL_WINDOWEVENT_RESIZED:
            hw_video_resize(0, 0);
            break;
        case SDL_WINDOWEVENT_EXPOSED:
            hw_video_update();
            break;
        case SDL_WINDOWEVENT_FOCUS_LOST:
            hw_mouse_ungrab();
            break;
        default:
            break;
    }
}

/* -------------------------------------------------------------------------- */

int main(int argc, char **argv)
{
    return main_1oom(argc, argv);
}

int hw_early_init(void)
{
    return 0;
}

int hw_init(void)
{
    int flags = SDL_INIT_VIDEO | (opt_audio_enabled ? SDL_INIT_AUDIO : 0);
    log_message("SDL_Init\n");
    if (SDL_Init(flags) < 0) {
        log_error("SDL_Init(0x%x) failed: %s\n", flags, SDL_GetError());
        return 11;
    }
    if (hw_audio_init()) {
        return 12;
    }
    return 0;
}

void hw_shutdown(void)
{
    hw_audio_shutdown();
    hw_video_shutdown();
    log_message("SDL_Quit\n");
    SDL_Quit();
}

void hw_textinput_start(void)
{
    SDL_StartTextInput();
    hw_textinput_active = true;
}

void hw_textinput_stop(void)
{
    SDL_StopTextInput();
    hw_textinput_active = false;
}

int hw_event_handle(void)
{
    SDL_Event e;

    SDL_PumpEvents();

    while (SDL_PollEvent(&e)) {
        switch (e.type) {
            uint32_t mod;
            case SDL_KEYDOWN:
                {
                    SDL_Keycode sym;
                    SDL_Keymod smod;
                    char c;
                    sym = e.key.keysym.sym;
                    smod = e.key.keysym.mod;
                    c = 0;
                    if (!(hw_kbd_check_hotkey(sym, smod, c))) {
                        mookey_t key;
                        if (sym & SDLK_SCANCODE_MASK) {
                            key = key_xlat_scan[SDLK_TBLI_FROM_SCAN(sym)];
                            c = 0;
                        } else {
                            key = key_xlat_key[sym];
                            c = (char)sym; /* TODO SDL 2 */
                            /* ignore ASCII range when expecting SDL_TEXTINPUT */
                            if (hw_textinput_active && ((key >= MOO_KEY_SPACE) && (key <= MOO_KEY_z))) {
                                key = MOO_KEY_LAST;
                            }
                        }
                        mod = mod_xlat(smod);
                        if (sym == SDLK_LCTRL || sym == SDLK_RCTRL) mod |= MOO_MOD_CTRL;
                        if ((key != MOO_KEY_UNKNOWN) && (key < MOO_KEY_LAST)) {
                            kbd_add_keypress(key, mod, c);
                        }
                        kbd_set_pressed(key, mod, true);
                    }
                }
                break;
            case SDL_KEYUP:
                {
                    SDL_Keycode sym;
                    SDL_Keymod smod;
                    mookey_t key;
                    sym = e.key.keysym.sym;
                    smod = e.key.keysym.mod;
                    mod = mod_xlat(smod);
                    if (sym == SDLK_LCTRL || sym == SDLK_RCTRL) mod &= ~MOO_MOD_CTRL;
                    if (sym & SDLK_SCANCODE_MASK) {
                        key = key_xlat_scan[SDLK_TBLI_FROM_SCAN(sym)];
                    } else {
                        key = key_xlat_key[sym];
                    }
                    kbd_set_pressed(key, mod, false);
                }
                break;
            case SDL_TEXTINPUT:
                if (hw_textinput_active && (e.text.text[0] != 0)) {
                    char c = e.text.text[0];
                    SDL_StopTextInput();
                    SDL_StartTextInput();
                    kbd_add_keypress(MOO_KEY_UNKNOWN, 0, c);
                }
                break;
            case SDL_MOUSEMOTION:
                if (hw_mouse_enabled) {
                    if (hw_opt_relmouse) {
                        hw_mouse_move((int)(e.motion.xrel), (int)(e.motion.yrel));
                    } else {
                        hw_mouse_set_xy(e.motion.x,e.motion.y);
                    }
                }
                break;
            case SDL_MOUSEBUTTONDOWN:
            case SDL_MOUSEBUTTONUP:
                hw_mouse_button((int)(e.button.button), (e.button.state == SDL_PRESSED));
                break;
            case SDL_MOUSEWHEEL:
                if (e.wheel.y != 0) {
                    hw_mouse_scroll((e.wheel.y > 0) ? -1 : 1);
                }
                break;
            case SDL_QUIT:
                hw_audio_shutdown_pre();
                exit(EXIT_SUCCESS);
                break;
            case SDL_WINDOWEVENT:
                if (e.window.windowID == hw_video_get_window_id()) {
                    hw_event_handle_window(&e.window);
                }
            default:
                break;
        }
    }
    if (hw_audio_check_process()) {
        exit(EXIT_FAILURE);
    }

    SDL_Delay(10);
    return 0;
}
